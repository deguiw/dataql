/*
 * Copyright 2002-2005 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.hasor.dataql.sqlproc.execute.config;
import net.hasor.cobble.setting.SettingNode;
import net.hasor.cobble.setting.data.TreeNode;
import net.hasor.dataql.sqlproc.dynamic.DynamicSql;
import net.hasor.dataql.sqlproc.dynamic.nodes.ArrayDynamicSql;
import net.hasor.dataql.sqlproc.dynamic.nodes.SelectKeyDynamicSql;
import net.hasor.dataql.sqlproc.execute.MultipleResultsType;

/**
 * SelectKey SqlConfig
 * @version : 2021-06-19
 * @author 赵永春 (zyc@hasor.net)
 */
public class QueryProcSql extends AbstractProcSql {
    private final SelectKeyProcSql selectKey;

    public QueryProcSql(DynamicSql target, SettingNode options) {
        super(target, options);
        this.selectKey = findSelectKey(target);
    }

    protected SelectKeyProcSql findSelectKey(DynamicSql target) {
        if (target instanceof ArrayDynamicSql) {
            for (DynamicSql dynamicSql : ((ArrayDynamicSql) target).getSubNodes()) {
                if (dynamicSql instanceof SelectKeyDynamicSql) {
                    SelectKeyDynamicSql skDynamicSql = (SelectKeyDynamicSql) dynamicSql;
                    //
                    SettingNode options = new TreeNode();
                    options.setValue("statementType", skDynamicSql.getStatementType());
                    options.setValue("timeout", String.valueOf(skDynamicSql.getTimeout()));
                    options.setValue("resultMap", skDynamicSql.getResultMap());
                    options.setValue("fetchSize", String.valueOf(skDynamicSql.getFetchSize()));
                    options.setValue("resultSetType", skDynamicSql.getResultSetType());
                    options.setValue("multipleResult", MultipleResultsType.LAST.getTypeName());
                    return new SelectKeyProcSql(skDynamicSql, options);
                }
            }
        }

        return null;
    }

    public SelectKeyProcSql getSelectKey() {
        return this.selectKey;
    }
}
